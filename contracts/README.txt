合约部分包含：

1. 工厂合约（PancakeFactory)
参考主网：（未找到）
测试网已部署：0x340786ca5cF95fBBf6C74496b383cC4947a933C0


2. wbnb （Wrapped BNB）
参考主网：https://bscscan.com/token/0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c
测试网已部署：
    0xF4ac4121d165679BaE22cEBdBB2FB005568fcd47


3. 路由合约（PancakeRouter）
参考主网：https://bscscan.com/address/0x10ed43c718714eb63d5aa57b78b54704e256024e#code

测试网已部署：
    Router01:
    0x4322142bf690CD5240f446417013c507F77C6e17


4. 主厨合约（MasterChef）
参考主网：https://testnet.bscscan.com/address/0x1d32c2945C8FDCBc7156c553B7cEa4325a17f4f9#code

测试网已部署：
    0x8207f69a918fb4954a39ddc02c94ae2a2051b1b9   （https://testnet.bscscan.com/address/0x8207f69a918fb4954a39ddc02c94ae2a2051b1b9#code）
    

